FROM golang:alpine
## We create an /app directory within our
## image that will hold our application source
## We copy everything in the root directory
## into our /app directory
WORKDIR /src

COPY ./src/. .

# RUN go mod init go-api-pipeline

# Copy and download dependency using go mod
RUN go get -d -v

RUN go build -o ./src/go-api-pipeline.exe

# Export necessary port
EXPOSE 8080


CMD ["./src/go-api-pipeline.exe"]