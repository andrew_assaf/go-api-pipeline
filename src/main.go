// main.go
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

// Coin - Our struct for all coins

type Coin struct {
	Status struct {
		Timestamp    time.Time   `json:"timestamp"`
		ErrorCode    int         `json:"error_code"`
		ErrorMessage interface{} `json:"error_message"`
		Elapsed      int         `json:"elapsed"`
		CreditCount  int         `json:"credit_count"`
		Notice       interface{} `json:"notice"`
		TotalCount   int         `json:"total_count"`
	} `json:"status"`
	Data []struct {
		ID                int         `json:"id"`
		Name              string      `json:"name"`
		Symbol            string      `json:"symbol"`
		Slug              string      `json:"slug"`
		NumMarketPairs    int         `json:"num_market_pairs"`
		DateAdded         time.Time   `json:"date_added"`
		Tags              []string    `json:"tags"`
		MaxSupply         int         `json:"max_supply"`
		CirculatingSupply int         `json:"circulating_supply"`
		TotalSupply       int         `json:"total_supply"`
		Platform          interface{} `json:"platform"`
		CmcRank           int         `json:"cmc_rank"`
		LastUpdated       time.Time   `json:"last_updated"`
		Quote             struct {
			Usd struct {
				Price            float64   `json:"price"`
				Volume24H        float64   `json:"volume_24h"`
				PercentChange1H  float64   `json:"percent_change_1h"`
				PercentChange24H float64   `json:"percent_change_24h"`
				PercentChange7D  float64   `json:"percent_change_7d"`
				PercentChange30D float64   `json:"percent_change_30d"`
				PercentChange60D float64   `json:"percent_change_60d"`
				PercentChange90D float64   `json:"percent_change_90d"`
				MarketCap        float64   `json:"market_cap"`
				LastUpdated      time.Time `json:"last_updated"`
			} `json:"USD"`
		} `json:"quote"`
	} `json:"data"`
}

var Coins []Coin

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to my Homepage!")
	fmt.Println("Endpoint Hit: homePage")
}

func returnAllCoins(w http.ResponseWriter, r *http.Request) {
	// url hitting to get data
	url := "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest"
	// make a request object
	req, err := http.NewRequest("GET", url, nil)
	// headers we need to set, first is authentication using coinmarket cap api key and second accepting json format
	req.Header.Set("X-CMC_PRO_API_KEY", "90645655-36a2-421c-8edc-f65f30d08523")
	req.Header.Set("Accept", "application/json")
	// create a http client
	client := &http.Client{}
	// client .go is sending the request
	res, err := client.Do(req)
	// checking if any erros
	if err != nil {
		fmt.Println(err)
	}
	// reading response into body object
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
	}
	// printing the body
	fmt.Println(string(body))
	// new variable called data which is based Coin
	var data Coin
	// extracting json and putting in data in variable from line 90
	json.Unmarshal([]byte(body), &data)
	// setting th headers again , setting on the response
	w.Header().Set("Content-Type", "application/json")
	//allows public to access api
	w.Header().Set("Access-Control-Allow-Origin", "*")
	// if successful
	w.WriteHeader(http.StatusOK)
	jsondata, err := json.Marshal(data)
	if err != nil {
		fmt.Println(err)
	}
	// writing back the data to the client
	w.Write(jsondata)

}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/coins", returnAllCoins)
	log.Fatal(http.ListenAndServe(":8000", myRouter))
}

func main() {

	handleRequests()
}
